#!/usr/bin/env python
# encoding: utf-8
"""multiprocesspool.py contain a class
to used all cores of a computer to do the same operation
on multiple elements of a list  
@author: Vezin Aurelien
@license: CeCILL-B"""

from multiprocessing import Lock, Queue, Value


class MultiProcessPool(object):
    """ ProcessPool allow you to do parallel computation in python
    @ivar operationfunction: the function used to do the same operation
    on any values. Must take a value of values as argument
    @type operationfunction: function
    @ivar queuefunction: the function use to do the same operation
    on any value calculated. Must take a calculated value as argument
    and a element of the return type as parameter
    @type queuefunction: function
    @ivar values: the list of value to use
    @type values: list(anything)
    @ivar valres: the result
    @type valres: anything
    @ivar q: the Queue
    @type q: Queue
    @ivar lock: the lock
    @type lock: Lock
    @ivar position: the current position in the list
    @type position: Value
    """
    
    def __init__(self, operationfunction, values, queuefunction, valres, celery=False):
        """ set all the class variable
        @param operationfunction: the function used to do the same 
        operation on any values. Must take a value of values as argument
        @type operationfunction: function
        @pram queuefunction: the function use to do the same operation
        on any value calculated. Must take a calculated value as argument
        and a element of the return type as parameter
        @type queuefunction: function
        @param values: the list of value to use
        @type values: list(anything)
        @param valres: the default result (if there is no values in the list)
        @type valres: anything
        """
        self.celery = celery
        self.__operationfunction = operationfunction
        self.__queuefunction = queuefunction
        self.__values = values
        self.__valres = valres
        self.__q = Queue()
        self.__lock = Lock()
        self.__position = Value('i', 0)
        
    def __processtarget(self):
        """ The method executed in each process """
        while self.__position.value < len(self.__values):
            #lock and get the current position in the list
            #then add 1 to the current position and unlock
            self.__lock.acquire()
            postemp = self.__position.value
            self.__position.value = self.__position.value +1
            self.__lock.release()
            #if we are in the boundary of the list use the function
            #created by the user and send the result in the queue
            if postemp < len(self.__values):
                res = self.__operationfunction(self.__values[postemp])
                self.__q.put(res)
    
    def __mapqueue(self):
        """  Save all the queue values in the way we want"""
        for i in range(0, len(self.__values)):
            self.__valres = self.__queuefunction(self.__q.get(), 
                                                 self.__valres)
            
    def run(self, nbprocess):
        """ do the computation
        @param nbprocess: the number of process to create
        @type nbprocess: int
        @return: the list result
        @rtype: list()"""
        if self.celery:
            from billiard.process import Process
            procs = [ Process(target=self.__processtarget, args=()) 
                     for i in range(0, nbprocess) ]
        else:
            from multiprocessing import Process
            procs = [ Process(target=self.__processtarget, args=()) 
                     for i in range(0, nbprocess) ]
        for p in procs:
            p.start()
        self.__mapqueue()
        for p in procs:
            p.join()
        return self.__valres

